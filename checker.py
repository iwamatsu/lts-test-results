import argparse
import yaml

def main() -> None:
    parser = argparse.ArgumentParser(description='LTP')

    parser.add_argument('testname', help='test name')
    parser.add_argument('--arch', help='arch')
    parser.add_argument('--kernel', help='kernel')
    parser.add_argument('--nodata', help='nodata', action='store_true')

    args = parser.parse_args()

    print ("- %s" % args.testname)
    with open('data.yaml') as file:
        reason = yaml.safe_load(file.read())

    if args.testname not in reason:
        print ("    - No data")
    else:
        if not args.nodata:
            data = reason[args.testname]
            #print (data)
            print ("    - reason:")
            for r in data["reason"]:
                print ("        - %s" % r)
            print ("    - reference:")
            for r in data["reference"]:
                print ("        - %s" % r)

if __name__ == "__main__":
    main()

